# lexxer

This is a clone of [lexic](https://code.google.com/archive/p/lexic/), a word finding game. You have a given amount of time to find as many words as 
possible from a grid by connnecting adjacent letters (click and drag mouse over letters). Points are given 
based on length of word. Whether an answer is a word or not is determined by comparison to a dictionary file.

Made in Java 8 with JavaFX. Dictionary search is done with [alphaSort](https://gitlab.com/jsheradin/alphaSort).

![screenshot](https://gitlab.com/jsheradin/lexxer/raw/master/screenshots/menu.PNG)

![screenshot](https://gitlab.com/jsheradin/lexxer/raw/master/screenshots/game.PNG)
